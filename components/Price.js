class Price extends React.Component {
    state = {
        currency: 'USD'
    }

    render() {
        let list = '';
        
        
        switch(this.state.currency) {
            case 'USD':
                list = <li className="list-group-item">{this.props.bitcoin.bpi.USD.description}
                        : <i className="fas fa-dollar-sign"></i>
                        <strong>{this.props.bitcoin.bpi.USD.rate}</strong>
                       </li>
                break; 
            case 'GBP':
                list = <li className="list-group-item">{this.props.bitcoin.bpi.GBP.description}
                        : <span className="badge badge-primary">{this.props.bitcoin.bpi.GBP.symbol}</span>
                        <strong>{this.props.bitcoin.bpi.GBP.rate}</strong>
                       </li>
                break; 
            case 'EUR':
                list = <li className="list-group-item">{this.props.bitcoin.bpi.EUR.description}
                        : <span className="badge badge-primary">{this.props.bitcoin.bpi.EUR.symbol}</span>
                        <strong>{this.props.bitcoin.bpi.EUR.rate}</strong>
                        </li>
                break; 
        } 

        return (
            <div className="row justify-content-md-center">
                <div className="col-md-12">
                    <h3>Price as of {this.props.bitcoin.time.updated}</h3>
                </div>
                <div className="col-md-4">
                    <ul className="list-group">
                        {list}
                    </ul>
                    <br />
                    <select onChange={e => this.setState({currency: e.target.value})} className="form-control">
                        <option value="USD">USD</option>
                        <option value="GBP">GBP</option>
                        <option value="EUR">EUR</option>
                    </select>
                </div>
            </div>
        );
    }
}

export default Price;