import Head from 'next/head';
import Navbar from "./Navbar";

const Layout = (props) => (
    <div>
        <Head>
            <title>Bitcoin Tracker</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cyborg/bootstrap.min.css" />
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        </Head>
        <Navbar />
        <div className="container-fluid">
            {props.children}
        </div>
    </div>
);

export default Layout;