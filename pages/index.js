import Fetch from 'isomorphic-unfetch';
import Price from '../components/Price';
import Layout from "../components/Layout";

const Index = (props) => (
    <Layout>
        <div className="text-center">
            <div className="jumbotron">
                <h2 className="display-3">Welcome to Bitcoin Price Tracker</h2>
                <p className="lead">A simple Web Application written in Next.js and developed by Arvin Kent S. Lazaga</p>    
            </div>
            <Price bitcoin={props.response} />
        </div>
    </Layout>
);

Index.getInitialProps = async function () {
    const res = await fetch('https://api.coindesk.com/v1/bpi/currentprice.json');
    const data = await res.json();

    return {
        response: data
    }
}

export default Index;
